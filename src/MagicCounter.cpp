// Default empty project template
#include "MagicCounter.hpp"

#include <stdlib.h>
#include <time.h>

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/Container>
#include <bb/cascades/ImageButton>
#include <bb/cascades/TouchEvent>
#include <bb/cascades/Label>
#include <bb/cascades/TranslateTransition>
#include <bb/cascades/ParallelAnimation>
#include <bb/cascades/InvokeQuery>

#include <bb/device/DisplayInfo>

#include <bb/system/SystemDialog>
#include <bb/system/SystemListDialog>

#include <Flurry.h>

using namespace bb::cascades;
using namespace bb::system;
using namespace bb::device;

MagicCounter::MagicCounter(bb::cascades::Application *app)
: QObject(app)
{
    rightPlayerScore = true;
    leftPlayerScore = true;

    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    qml->setContextProperty("MagicCounter", this);

    // create root object for the UI
    root = qml->createRootObject<AbstractPane>();

    centerContainer = root->findChild<Container*>("centerContainer");

    leftPoisonContainer = root->findChild<Container*>("leftPoisonContainer");
    rightPoisonContainer = root->findChild<Container*>("rightPoisonContainer");
    leftScoreContainer = root->findChild<Container*>("leftScoreContainer");
    rightScoreContainer = root->findChild<Container*>("rightScoreContainer");

    QObject::connect(leftPoisonContainer, SIGNAL(touch(bb::cascades::TouchEvent*)),
        	this, SLOT(leftPoisonTouched(bb::cascades::TouchEvent*)));

    QObject::connect(rightPoisonContainer, SIGNAL(touch(bb::cascades::TouchEvent*)),
            this, SLOT(rightPoisonTouched(bb::cascades::TouchEvent*)));

    QObject::connect(leftScoreContainer, SIGNAL(touch(bb::cascades::TouchEvent*)),
            this, SLOT(leftScoreTouched(bb::cascades::TouchEvent*)));

    QObject::connect(rightScoreContainer, SIGNAL(touch(bb::cascades::TouchEvent*)),
            this, SLOT(rightScoreTouched(bb::cascades::TouchEvent*)));

    listDialog = root->findChild<SystemListDialog*>("diceDialog");
    QObject::connect(listDialog, SIGNAL(finished(bb::system::SystemUiResult::Type)),
            this, SLOT(onDialogFinished(bb::system::SystemUiResult::Type)));

    manageSettings();

    // set created root object as a scene
    app->setScene(root);
}

void MagicCounter::manageSettings()
{
	QString total_runs_string = this->getValueFor("total_runs", "0");
	int total_runs = total_runs_string.toInt();

	qDebug() << "Total Runs: " << total_runs;

	if(total_runs != -1)
	{
		total_runs++;
	}

	if(total_runs == 3)
	{
		//show annoying prompt
		this->showRateMyApplication();

		//never show it again.
		total_runs = -1;
	}

	this->saveValueFor("total_runs", QString::number(total_runs));
	qDebug() << "After: " << this->getValueFor("total_runs", "Fucked");
}

void MagicCounter::leftPoisonTouched(bb::cascades::TouchEvent* e)
{
	if(e->touchType() == TouchType::Up)
	{
		leftPlayerScore = leftPlayerScore ? false : true;
		swapPoisonAndLifeOnLeft();
	}
}

void MagicCounter::rightPoisonTouched(bb::cascades::TouchEvent* e)
{
	if(e->touchType() == TouchType::Up)
	{
		rightPlayerScore = rightPlayerScore ? false : true;
		swapPoisonAndLifeOnRight();
	}
}

void MagicCounter::leftScoreTouched(bb::cascades::TouchEvent* e)
{
	if(e->touchType() == TouchType::Up)
	{
		//log flurry
		Flurry::Analytics::LogEvent("SWITCHED_TO_POISON_LEFT", 0);

		leftPlayerScore = leftPlayerScore ? false : true;
		swapPoisonAndLifeOnLeft();
	}
}

void MagicCounter::rightScoreTouched(bb::cascades::TouchEvent* e)
{
	if(e->touchType() == TouchType::Up)
	{
		//log flurry
		Flurry::Analytics::LogEvent("SWITCHED_TO_POISON_RIGHT", 0);

		rightPlayerScore = rightPlayerScore ? false : true;
		swapPoisonAndLifeOnRight();
	}
}

void MagicCounter::swapPoisonAndLifeOnRight()
{
	ParallelAnimation *scoreAndLifeTransitionSeparate = ParallelAnimation::create()
	    .add(TranslateTransition::create(rightScoreContainer)
	        .toX(20).duration(500))
	    .add(TranslateTransition::create(rightPoisonContainer)
	        .toX(-72).duration(500))
	    .autoDeleted();

    QObject::connect(scoreAndLifeTransitionSeparate, SIGNAL(ended()),
				this, SLOT(onRightPoisonAndLifeSeparated()));
    scoreAndLifeTransitionSeparate->play();
}

void MagicCounter::swapPoisonAndLifeOnLeft()
{
	ParallelAnimation *scoreAndLifeTransitionSeparate = ParallelAnimation::create()
		.add(TranslateTransition::create(leftScoreContainer)
			.toX(-20).duration(500))
		.add(TranslateTransition::create(leftPoisonContainer)
			.toX(72).duration(500))
		.autoDeleted();

	QObject::connect(scoreAndLifeTransitionSeparate, SIGNAL(ended()),
				this, SLOT(onLeftPoisonAndLifeSeparated()));
	scoreAndLifeTransitionSeparate->play();
}

void MagicCounter::onLeftPoisonAndLifeSeparated()
{
	int score = centerContainer->indexOf(leftScoreContainer);
	int poison = centerContainer->indexOf(leftPoisonContainer);
	centerContainer->swap(score, poison);

	ParallelAnimation *scoreAndLifeTransitionComeTogether = ParallelAnimation::create()
		.add(TranslateTransition::create(leftScoreContainer)
			.toX(0).duration(500))
		.add(TranslateTransition::create(leftPoisonContainer)
			.toX(0).duration(500))
		.autoDeleted();
	scoreAndLifeTransitionComeTogether->play();
}

void MagicCounter::onRightPoisonAndLifeSeparated()
{
	int score = centerContainer->indexOf(rightScoreContainer);
	int poison = centerContainer->indexOf(rightPoisonContainer);
	centerContainer->swap(score, poison);

	ParallelAnimation *scoreAndLifeTransitionComeTogether = ParallelAnimation::create()
	    .add(TranslateTransition::create(rightScoreContainer)
	        .toX(0).duration(500))
	    .add(TranslateTransition::create(rightPoisonContainer)
	        .toX(0).duration(500))
	    .autoDeleted();
	scoreAndLifeTransitionComeTogether->play();
}

bool MagicCounter::leftScoreSet()
{
	return this->leftPlayerScore;
}

bool MagicCounter::rightScoreSet()
{
	return this->rightPlayerScore;
}

void MagicCounter::onDialogFinished(bb::system::SystemUiResult::Type type)
{
	if(type != SystemUiResult::ConfirmButtonSelection)
	{
		return;
	}

	QList<int> selectedItems = listDialog->selectedIndices();
	if(selectedItems.isEmpty()) return;
	int selected = selectedItems[0];

	//Seed the random number generator.
	srand(time(NULL));
	int randInt = rand();
	QString randStringFull;
	qint32 myRand = randInt % 2 + 1;
	QString randString;
	QString typeString;
	if(selected == 0)
	{
		randString = QString::number(myRand);
		typeString = "H/T - ";
		randString = (randString == "1") ? "Heads" : "Tails";
		randStringFull.append(randString);
		randStringFull.append("\n");
	}
	else if(selected == 1)
	{
        myRand = randInt % 6 + 1;
        randString = QString::number(myRand);
        typeString= "D6 - ";
        randStringFull.append(randString);
        randStringFull.append("\n");
	}
	else if(selected == 2)
	{
        myRand = randInt % 12 + 1;
        randString = QString::number(myRand);
        typeString = "D12 - ";
        randStringFull.append(randString);
        randStringFull.append("\n");
	}
	else if(selected == 3)
	{
        myRand = randInt % 20 + 1;
        randString = QString::number(myRand);
        typeString = "D20 - ";
        randStringFull.append(randString);
        randStringFull.append("\n");
	}


	SystemDialog *resultDialog = new SystemDialog();
	resultDialog->setBody(randStringFull);
	resultDialog->setTitle(typeString + "Dice Roll Result");
	resultDialog->show();
}


//Settings functions (invokable from QML)
QString MagicCounter::getValueFor(const QString &objectName, const QString &defaultValue)
{
    QSettings settings;

    // If no value has been saved, return the default value.
    if (settings.value(objectName).isNull()) {
        return defaultValue;
    }

    // Otherwise, return the value stored in the settings object.
    return settings.value(objectName).toString();
}

void MagicCounter::saveValueFor(const QString &objectName, const QString &inputValue)
{
    // A new value is saved to the application settings object.
    QSettings settings;
    settings.setValue(objectName, QVariant(inputValue));
}

// A public function to display a SystemDialog in your UI
void MagicCounter::showRateMyApplication() {

    // Set up the SystemDialog with a title, some body text.
    // Label the two standard buttons with specific text.
    // Add a custom button for fun.

    SystemDialog *dialog = new SystemDialog("Review App", "Maybe Later");

    dialog->setTitle("Support Magic Counter?");
    dialog->setBody("Would you like to leave a review for Magic Counter on"
    		" BlackBerry World? Reviews are immensely helpful to the app."
    		" If you've already left a review your help is much appreciated. "
    		" You will only see this message once!");
    dialog->setDismissAutomatically(true);

    // Connect your functions to handle the predefined signals for the buttons.
    // The slot will check the SystemUiResult to see which button was clicked.

    bool success = connect(dialog,
        SIGNAL(finished(bb::system::SystemUiResult::Type)),
        this,
        SLOT(onRateMyAppDialogFinished(bb::system::SystemUiResult::Type)));

    if (success) {        // Signal was successfully connected.
        // Now show the dialog box in your UI
        dialog->show();
    } else {
        // Failed to connect to signal.
        dialog->deleteLater();
    }
}


void MagicCounter::onRateMyAppDialogFinished(bb::system::SystemUiResult::Type type)
{
	if(type == SystemUiResult::ConfirmButtonSelection)
	{
		m_RateApplicationInvocation = Invocation::create(
			InvokeQuery::create()
			  .invokeTargetId("sys.appworld")
			  .uri("appworld://content/21798031")
			  .invokeActionId("bb.action.OPEN"));
		QObject::connect(m_RateApplicationInvocation, SIGNAL(armed()),
			this, SLOT(openAppReviewPageArmed()));
		QObject::connect(m_RateApplicationInvocation, SIGNAL(finished()),
				m_RateApplicationInvocation, SLOT(deleteLater()));
	}
}

void MagicCounter::openAppReviewPageArmed()
{
	m_RateApplicationInvocation->trigger("bb.action.OPEN");
}

int MagicCounter::getScreenWidth()
{
	bb::device::DisplayInfo display;
	qDebug() << "width: " << display.pixelSize().width();
	return display.pixelSize().width();
}
