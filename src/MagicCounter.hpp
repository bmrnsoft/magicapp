
#ifndef MagicCounter_HPP_
#define MagicCounter_HPP_

#include <QObject>
#include <bb/cascades/Container>
#include <bb/cascades/AbstractPane>
#include <bb/system/SystemUiResult>
#include <bb/system/SystemListDialog>
#include <bb/cascades/Invocation>


using namespace bb::cascades;
using namespace bb::system;

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class MagicCounter : public QObject
{
    Q_OBJECT
public:
    MagicCounter(bb::cascades::Application *app);
    virtual ~MagicCounter() {}

    Q_INVOKABLE
    bool leftScoreSet();
    Q_INVOKABLE
    bool rightScoreSet();

    void swapPoisonAndLifeOnRight();
    void swapPoisonAndLifeOnLeft();

    Q_INVOKABLE
    int getScreenWidth();

public slots:

	Q_INVOKABLE
	QString getValueFor(const QString &objectName, const QString &defaultValue);

	Q_INVOKABLE
	void saveValueFor(const QString &objectName, const QString &inputValue);

	void onLeftPoisonAndLifeSeparated();
	void onRightPoisonAndLifeSeparated();

	void leftPoisonTouched(bb::cascades::TouchEvent* e);
	void rightPoisonTouched(bb::cascades::TouchEvent* e);
	void leftScoreTouched(bb::cascades::TouchEvent* e);
	void rightScoreTouched(bb::cascades::TouchEvent* e);
	void onDialogFinished(bb::system::SystemUiResult::Type type);

private:

	bool rightPlayerScore;
	bool leftPlayerScore;

	AbstractPane *root;

	Container* centerContainer;

    Container* leftPoisonContainer;
    Container* rightPoisonContainer;
    Container* leftScoreContainer;
    Container* rightScoreContainer;

    SystemListDialog* listDialog;

    Invocation *m_RateApplicationInvocation;
    void manageSettings();
    void showRateMyApplication();

private slots:
	void onRateMyAppDialogFinished(bb::system::SystemUiResult::Type type);
	void openAppReviewPageArmed();
};


#endif /* MagicCounter_HPP_ */
