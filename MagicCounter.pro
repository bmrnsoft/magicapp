APP_NAME = MagicCounter

CONFIG += qt warn_on cascades10

LIBS += -lbbsystem -lscreen -lcrypto -lcurl -lpackageinfo -lbbdevice

include(config.pri)
