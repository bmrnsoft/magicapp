// Default empty project template
import bb.cascades 1.0
import bb.system 1.0

// creates one page with a label
Page {    
    Container {
        layout: DockLayout {}
        minHeight: 720  
        background: Color.Black  
        
	    Container {
	        layout: DockLayout {}
	        minHeight: 720
	        verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            
            background: back.imagePaint
            attachedObjects: [
                ImagePaintDefinition {
                    id: back
                    repeatPattern: RepeatPattern.Fill
                    imageSource: "asset:///images/seamlesstexture2_500.jpg"
                }
            ]        
	        
	        //Top            
            Container {            
	            verticalAlignment: VerticalAlignment.Top
	            horizontalAlignment: HorizontalAlignment.Center
	            
	            layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
	            
	            //The 6 up images.                
                ImageButton
	            {
	                id:leftUp1Button
	                
	                layoutProperties: StackLayoutProperties {

	                }
	                
                    defaultImageSource: "asset:///images/Arrow1_up_unpressed.png"
                    onClicked: {
                        if(MagicCounter.leftScoreSet())
                        {                            
                            var score = parseInt(leftPlayerScore.text);
                            score = score + 1;
                            leftPlayerScore.text = score;
                        }
                        else
                        {
                            var poison = parseInt(leftPlayerPoison.text);
	                        poison = poison + 1;
	                        leftPlayerPoison.text = poison;
                        }
                    }                    
                }
	            
	            Container {
                    leftPadding: 10
                    ImageButton
		            {
		                id:leftUp3Button
	                    defaultImageSource: "asset:///images/Arrow3_up_unpressed.png"
	                    onClicked: {
		                    if(MagicCounter.leftScoreSet())
	                        {                            
	                            var score = parseInt(leftPlayerScore.text);
	                            score = score + 3;
	                            leftPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(leftPlayerPoison.text);
		                        poison = poison + 3;
		                        leftPlayerPoison.text = poison;
	                        }
		                }
	                }
		        }
                Container {
                    leftPadding: 38
                    visible: MagicCounter.getScreenWidth() != 720
                    ImageButton
		            {
		                id:leftUp5Button
	                    defaultImageSource: "asset:///images/Arrow5_up_unpressed.png"
	                    onClicked: {
		                    if(MagicCounter.leftScoreSet())
	                        {                            
	                            var score = parseInt(leftPlayerScore.text);
	                            score = score + 5;
	                            leftPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(leftPlayerPoison.text);
		                        poison = poison + 5;
		                        leftPlayerPoison.text = poison;
	                        }
		                }
	                    leftMargin: 0.0
	                }
                 }
                
                Container 
	            {
	                layoutProperties: StackLayoutProperties {
	                    spaceQuota: 1
	                }
	            }
                Container {
                    rightPadding: 38
                    visible: MagicCounter.getScreenWidth() != 720
                    ImageButton
		            {
		                id:rightUp5Button
	                    defaultImageSource: "asset:///images/Arrow5_up_unpressed.png"
	                    onClicked: {
		                    if(MagicCounter.rightScoreSet())
	                        {                            
	                            var score = parseInt(rightPlayerScore.text);
	                            score = score + 5;
	                            rightPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(rightPlayerPoison.text);
		                        poison = poison + 5;
		                        rightPlayerPoison.text = poison;
	                        }
		                }
	                }
                 }

                Container {
                    rightPadding: 15
	                ImageButton
		            {
		                id:rightUp3Button
	                    onClicked: {
		                    if(MagicCounter.rightScoreSet())
	                        {                            
	                            var score = parseInt(rightPlayerScore.text);
	                            score = score + 3;
	                            rightPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(rightPlayerPoison.text);
		                        poison = poison + 3;
		                        rightPlayerPoison.text = poison;
	                        }
		                }
	                    defaultImageSource: "asset:///images/Arrow3_up_unpressed.png"
	                }
                 }
	            
	            ImageButton
	            {
	                id:rightUp1Button
	                
	                layoutProperties: StackLayoutProperties {
	                    spaceQuota: -1
	                }
                    onClicked: {
	                    if(MagicCounter.rightScoreSet())
                        {                            
                            var score = parseInt(rightPlayerScore.text);
                            score = score + 1;
                            rightPlayerScore.text = score;
                        }
                        else
                        {
                            var poison = parseInt(rightPlayerPoison.text);
	                        poison = poison + 1;
	                        rightPlayerPoison.text = poison;
                        }
	                }
                    defaultImageSource: "asset:///images/Arrow1_up_unpressed.png"
                }	            	            
	        }
            
            ImageView {
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                imageSource: "asset:///images/VerticalLine.png"
            }
	        
	        //Center
	        Container {
	            id: centerContainer
	            objectName: "centerContainer"
	            
	            layout: DockLayout {}
	            verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Left
                
                preferredHeight: 290;                            
	            preferredWidth: 1280;
	            leftPadding: 25
	            rightPadding: 25
	            
	            //LeftPoison
	            Container
	            {
	                id:leftPoisonContainer
	                objectName: "leftPoisonContainer"
	                	            
	                verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
	                leftPadding: MagicCounter.getScreenWidth() != 720 ? 150 /*Z10*/ : 100 /*Q10 & Q5*/
                    //LeftPoison
		            Container {
		                layout: DockLayout {} 
		                
		                preferredHeight: MagicCounter.getScreenWidth() != 720 ? 180 /*Z10*/ : 140 /*Q10 & Q5*/
		                preferredWidth: MagicCounter.getScreenWidth() != 720 ? 180 /*Z10*/ : 140 /*Q10 & Q5*/
                        //background: Color.DarkGreen
		                
		                Label
                        {
                            id: leftPlayerPoison
                            
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            
                            text: "0"
                            textStyle.fontSizeValue: 24.0
	                        textStyle.fontSize: FontSize.PointValue
                        }
                        
                        background: poisonBack1.imagePaint
                            attachedObjects: [
                                ImagePaintDefinition {
                                    id: poisonBack1
                                    repeatPattern: RepeatPattern.Fill
                                    imageSource: "asset:///images/PoisonBox.png"
                                }
                            ]                        
	                    
	                }
                }
	            
	            //LeftScore
	            Container 
	            {
	                id:leftScoreContainer
	                objectName: "leftScoreContainer"
	                
	                layout: DockLayout {} 
	                
	                preferredHeight: MagicCounter.getScreenWidth() != 720 ? 240 /*Z10*/ : 200 /*Q10 & Q5*/
	                preferredWidth: MagicCounter.getScreenWidth() != 720 ? 240 /*Z10*/ : 200 /*Q10 & Q5*/

                    //background: Color.Red
	                	                
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left

                    Label
                    {
                        id: leftPlayerScore
                        text: "20"                        
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        
                        textStyle.fontSizeValue: 32.0
                        textStyle.fontSize: FontSize.PointValue
                    }
                    
                    background: lifeBack1.imagePaint
                        attachedObjects: [
                            ImagePaintDefinition {
                                id: lifeBack1
                                repeatPattern: RepeatPattern.Fill
                                imageSource: "asset:///images/LifeBox.png"
                            }
                        ] 
                }
                                
                Container
                {
                    id: centerButtonContainer
                    objectName:"centerButtonContainer"
                    
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    
                    Container
                    {
                        background: reload.imagePaint
                        
                        attachedObjects: [
                            ImagePaintDefinition {
                                id: reload
                                repeatPattern: RepeatPattern.Fill
                                imageSource: "asset:///images/seamlesstexture29_500_rounded.png"
                            }
                        ]
	                    ImageButton
	                    {
	                        defaultImageSource: "asset:///images/reload.png"
	                        onClicked: {
	                            rightPlayerScore.text = "20";
	                            leftPlayerScore.text = "20";
	                            rightPlayerPoison.text = "0";
	                            leftPlayerPoison.text="0";
	                            //restartDialog.open();
	                        }
	                        attachedObjects: [
                                RestartDialog{
                                    id: restartDialog
                                }
                            ]
	                    }
	                }
                    Container
                    {
                        minHeight: 20
                    }
                    Container
                    {
                        layout: DockLayout {
                                
                        }
			            verticalAlignment: VerticalAlignment.Center
			            horizontalAlignment: HorizontalAlignment.Center
                        background: dice.imagePaint
                        
                        attachedObjects: [
                            ImagePaintDefinition {
                                id: dice
                                repeatPattern: RepeatPattern.Fill
                                imageSource: "asset:///images/seamlesstexture29_500_rounded.png"
                            }
                        ]
                        
	                    ImageButton
	                    {
	                        property bool created : false;
	                        defaultImageSource: "asset:///images/dice.png"
	                        objectName: "diceButton"

                            onClicked: {
                                if(!created)
                            	{
                                    diceDialog.appendItem("H/T")
                                    diceDialog.appendItem("D6")
                                    diceDialog.appendItem("D12")
                                    diceDialog.appendItem("D20")
                                    created = true;
                                }
                                diceDialog.show()
                            }
                            attachedObjects: [
                                SystemListDialog {                                    
                                    id: diceDialog
                                    objectName: "diceDialog"
                                    title: qsTr("Dice Roll")                                    
                                }
                            ]
                        	
	                    }
	                }                    
                }
	            
	            //Right Poison
	            Container	            
	            {
	                id:rightPoisonContainer
	                objectName:"rightPoisonContainer"
	                
	                verticalAlignment: VerticalAlignment.Center
	            	horizontalAlignment: HorizontalAlignment.Right
	                rightPadding: MagicCounter.getScreenWidth() != 720 ? 150 /*Z10*/ : 100 /*Q10 & Q5*/
                    //RightPoison
		            Container {		                
		                layout: DockLayout {} 
		                
		                //DELME
                        preferredHeight: MagicCounter.getScreenWidth() != 720 ? 180 /*Z10*/ : 140 /*Q10 & Q5*/
                        preferredWidth: MagicCounter.getScreenWidth() != 720 ? 180 /*Z10*/ : 140 /*Q10 & Q5*/
                        //background: Color.DarkGreen
		                
		                Label
                        {
                            id: rightPlayerPoison
                            
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            
                            text: "0"
	                        textStyle.fontSizeValue: 24.0
	                        textStyle.fontSize: FontSize.PointValue              
                        }
                        
    	                background: poisonBack2.imagePaint
    	                    attachedObjects: [
    	                        ImagePaintDefinition {
    	                            id: poisonBack2
    	                            repeatPattern: RepeatPattern.Fill
    	                            imageSource: "asset:///images/PoisonBox.png"
    	                        }
    	                    ] 
	                }
	            }	        
	            
	            //RightScore
	            Container {
	                id:rightScoreContainer
	                objectName:"rightScoreContainer"
	                
	                layout: DockLayout {} 
	                
	                //DELME
                    preferredHeight: MagicCounter.getScreenWidth() != 720 ? 240 /*Z10*/ : 200 /*Q10 & Q5*/
                    preferredWidth: MagicCounter.getScreenWidth() != 720 ? 240 /*Z10*/ : 200 /*Q10 & Q5*/
                    //background: Color.Red	                                	                
	                
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Right
                    
                    Label
                    {
                        id: rightPlayerScore
                        text: "20"                        
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        
                        textStyle.fontSizeValue: 32.0
                        textStyle.fontSize: FontSize.PointValue
                    }                    
                
	                background: lifeBack2.imagePaint
	                    attachedObjects: [
	                        ImagePaintDefinition {
	                            id: lifeBack2
	                            repeatPattern: RepeatPattern.Fill
	                            imageSource: "asset:///images/LifeBox.png"
	                        }
	                    ] 
                }	            
	    
	        }
	        
	        //Bottom
	        Container {
	            verticalAlignment: VerticalAlignment.Bottom
	            horizontalAlignment: HorizontalAlignment.Center
	            
	            layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
	            
	            //The 6 down images.
                ImageButton
	            {
	                id:leftDown1Button
	                
	                //layoutProperties: StackLayoutProperties {
                    //    spaceQuota: -1
                    //}
                    
                    defaultImageSource: "asset:///images/Arrow1_down_unpressed.png"
                    
                    onClicked: {
                        if(MagicCounter.leftScoreSet())
                        {                            
                            var score = parseInt(leftPlayerScore.text);
                            score = score - 1;
                            leftPlayerScore.text = score;
                        }
                        else
                        {
                            var poison = parseInt(leftPlayerPoison.text);
	                        poison = poison - 1;
	                        leftPlayerPoison.text = poison;
                        }
                    } 
                }

                Container {
                    leftPadding: 15
                    topPadding: 25
	                ImageButton
		            {
		                id:leftDown3Button
	                    defaultImageSource: "asset:///images/Arrow3_down_unpressed.png"
	                    
	                    onClicked: {
	                        if(MagicCounter.leftScoreSet())
	                        {                            
	                            var score = parseInt(leftPlayerScore.text);
	                            score = score - 3;
	                            leftPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(leftPlayerPoison.text);
		                        poison = poison - 3;
		                        leftPlayerPoison.text = poison;
	                        }
	                    } 
	                }
                 }
                Container {
                    leftPadding: 38
                    topPadding: 50
                    visible: MagicCounter.getScreenWidth() != 720
	                ImageButton
		            {
		                id:leftDown5Button
	                    defaultImageSource: "asset:///images/Arrow5_down_unpressed.png"
	                    
	                    onClicked: {
	                        if(MagicCounter.leftScoreSet())
	                        {                            
	                            var score = parseInt(leftPlayerScore.text);
	                            score = score - 5;
	                            leftPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(leftPlayerPoison.text);
		                        poison = poison - 5;
		                        leftPlayerPoison.text = poison;
	                        }
	                    } 
	                }
	            }
	            
	            Container 
	            {
	                layoutProperties: StackLayoutProperties {
	                    spaceQuota: 1
	                }
	            }

                Container {
                    rightPadding: 38
                    topPadding: 50
                    visible: MagicCounter.getScreenWidth() != 720
                    ImageButton
		            {
		                id:rightDown5Button
	                    defaultImageSource: "asset:///images/Arrow5_down_unpressed.png"
	                    
	                    onClicked: {
	                        if(MagicCounter.rightScoreSet())
	                        {                            
	                            var score = parseInt(rightPlayerScore.text);
	                            score = score - 5;
	                            rightPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(rightPlayerPoison.text);
		                        poison = poison - 5;
		                        rightPlayerPoison.text = poison;
	                        }
	                    }
	                }
                }
                
                Container {
                    rightPadding: 15
                    topPadding: 25
	                ImageButton
		            {
		                id:rightDown3Button
	                    defaultImageSource: "asset:///images/Arrow3_down_unpressed.png"
	                    
	                    onClicked: {
	                        if(MagicCounter.rightScoreSet())
	                        {                            
	                            var score = parseInt(rightPlayerScore.text);
	                            score = score - 3;
	                            rightPlayerScore.text = score;
	                        }
	                        else
	                        {
	                            var poison = parseInt(rightPlayerPoison.text);
		                        poison = poison - 3;
		                        rightPlayerPoison.text = poison;
	                        }
	                    }
	                }
                }
	            
	            ImageButton
	            {
	                id:rightDown1Button
	                
	                //layoutProperties: StackLayoutProperties {
                    //    spaceQuota: -1
                    //}
                    defaultImageSource: "asset:///images/Arrow1_down_unpressed.png"
                    
                    onClicked: {
                        if(MagicCounter.rightScoreSet())
                        {                            
                            var score = parseInt(rightPlayerScore.text);
                            score = score - 1;
                            rightPlayerScore.text = score;
                        }
                        else
                        {
                            var poison = parseInt(rightPlayerPoison.text);
	                        poison = poison - 1;
	                        rightPlayerPoison.text = poison;
                        }
                    }
                }	                        
	        }
	    }	
    }
}

