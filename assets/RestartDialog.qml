import bb.cascades 1.0

Dialog {     
    Container {
        // The Dialog does not automatically fill the entire screen
        // like a Page does, so in order for it to be possible to center
        // the dialog box on screen, the width and height must be set.
 
        preferredWidth: 1280
        preferredHeight: 768
         
        // The background is set to semi-transparent to indicate
        // that it is not possible to interact with the screen
        // behind the dialog box.
 
        background: Color.create (0.0, 0.0, 0.0, 0.5)
 
        layout: DockLayout {
        }
 
        Container {
            maxHeight: 397
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center           
            layout: DockLayout {
            }
 
            ImageView {
                imageSource: "assets:///images/seamlesstexture29_500_rounded.png"
            }
             
            Container {
                topPadding: 5
                bottomPadding: 23
                leftPadding: 23
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                 
                Label {
                    text: ""
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                    textStyle.color: Color.create("#fafafa")
                    horizontalAlignment: HorizontalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                 
                Label {
                    text: "BEEP! BEEP! BEEP! "
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 2.5
                    }
                }
                 
                ToggleButton {
                    id: fireAlarm
                    checked: true
                    horizontalAlignment: HorizontalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
 
                    onCheckedChanged: {
                        // Close the dialog when the toggle button
                        // turns the fire alarm off.
                        if(!checked) {
                            mydialog.close()
                        }
                    }
                } // ToggleButton
            } // Container
        } // Container
    } // Container
 
    onOpened: {
        // Reset the fire alarm since it is opened.
        fireAlarm.checked = true;
    }
} // Dialog